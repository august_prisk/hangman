#See if we can refactor some of the methods to make them more readable
#Look into what private methods are how they work, what would be useful to take out of public methods and put in private methods and why we would do that
#Think about if i need to test private methods or not
#Hint: probably don't need to value the variable assignment as heavy as what the public methods do or what they produce
#Ensure the class makes sense and that they are working correctly and then refactor the code and ensure they still pass the tests

class Game
  attr_reader :word, :num_guess

  def initialize(word = "chocolate", num_guess = 5)
    @word = word
    @word_clues = ""
    @word.length.times { @word_clues << '_'}
    @guesses = []
    @num_guess = num_guess
  end

  def display
    print "letters guessed: \n"
    puts "Incorrect guesses remaining: #{@num_guess}"
    @guesses.each do |guess|
      print guess.to_s + ' ' 
    end
    puts
    puts @word_clues
  end

  def guess
    begin
      display
      print "Guess a letter"
      puts
      guessed = gets.chomp
      raise "Invalid input: #{guessed}" unless /[[:alpha:]]/.match(guessed) && guessed.length == 1
      raise "you've already guessed that letter!" if @guesses.include?(guessed)
      enter_guess(guessed.downcase)
    rescue => exception
      puts
      puts exception.to_s
      retry
    end

  end

  def over?
    if @word == @word_clues
      puts "You guessed the word!"
      puts @word
      return true
    elsif @num_guess.zero?
      puts "You've run out of guesses."
      puts "The correct word was #{@word}"
      return true
    end
    false
  end

  private

  def valid?(char)

  end

  def included?(char)

  end

  def enter_guess(char)
    @guesses << char
    if @word.include?(char)
      add_clue(char)
      puts "Good guess!"
    else
      @num_guess -= 1
      puts "That's wrong!"
    end
  end

  def add_clue(char)
    @word.split('').each_with_index do |v, i|
      @word_clues[i] = char if v == char
    end
  end
end