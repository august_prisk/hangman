require_relative 'game'

def generate_word
  dictionary = File.open('5desk.txt', 'r')
  words = []
    dictionary.each_line do |l|
      l = l.chomp
      if l.length <= 12 &&
         l.length >= 5 &&
         /[[:lower:]]/.match(l[0])
        words << l
      end
    end
  dictionary.close
  words.sample
end

@word = generate_word

game = Game.new(@word, @word.length-2)

until game.over?
  game.guess
end
