require 'spec_helper'
require_relative '../lib/game'

RSpec.describe Game do
  #Look into shared examples and assess for myself if its more readable to use shared examples than the current methods
  #Understand how to dynamically send methods to the class that are defined within the context
  #Look into dynamically sending parameters to this class and assesss for myself if that is more readable than what I am doing now
  #Assert what my test coverage is like now are there any inconsistencies between the two context blocks
  #Is there any other testing I can do on this class.
  context "initialized with no parameters " do
    let(:game) { described_class.new }

    it "returns false that the game is over on initialization" do
      expect(game.over?).to eq false
    end

    it "expects default word to equal chocolate" do
      expect(game.word).to eq "chocolate"
    end
  end

  
  context "initialized with parameters" do 
    let(:game) { described_class.new("tetris", 10) }

    it "can change the default word" do
      expect(game.word).to eq "tetris"
    end

    it "can change the default number of guesses" do
      expect(game.num_guess).to eq 10
    end
  end
end